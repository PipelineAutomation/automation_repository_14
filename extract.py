#!/usr/bin/python

'''
Reads the specified file and looks for the following match string
<ReleaseVersion>(x.x.x)</ReleaseVersion>
Prints the version to console
'''

import re
import sys
if len(sys.argv) > 1:
    with open(sys.argv[1], 'r') as testfile:
        match = re.search(r"<ReleaseVersion>([0-9*][.][0-9*][.][0-9*])<\/ReleaseVersion>", testfile.read())
        if match:
            print(match.groups(1)[0])