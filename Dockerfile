FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app
# Copy everything else and build
COPY . .
WORKDIR /app/automation_repository_14.Api
RUN dotnet restore
RUN dotnet publish -c Release -o ../out
# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
# Change timezone to local time
ENV TZ=Europe/Copenhagen
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
WORKDIR /app
COPY --from=build-env /app/out .
EXPOSE 80
CMD ["dotnet", "automation_repository_14.Api.dll"]